package api.service;

import api.entity.Transaction;
import org.javamoney.moneta.Money;

public interface TransferService {

    Transaction transferBetweenBankAccounts(String fromBankAccountNumber, String toBankAccountNumber, Money amount);
}
