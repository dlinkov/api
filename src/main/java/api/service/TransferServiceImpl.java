package api.service;

import api.entity.*;
import api.exception.BankAccountNotFoundException;
import api.repository.BankAccountBalanceOperationRepository;
import api.repository.BankAccountRepository;
import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Component
public class TransferServiceImpl implements TransferService {

    private final BankAccountRepository bankAccountRepository;
    private final BankAccountBalanceOperationRepository bankAccountBalanceOperationRepository;
    private final BankAccountBalanceOperationFactory bankAccountBalanceOperationFactory;
    private final TransactionFactory transactionFactory;

    @Autowired
    public TransferServiceImpl(BankAccountRepository bankAccountRepository,
            BankAccountBalanceOperationRepository bankAccountBalanceOperationRepository,
            BankAccountBalanceOperationFactory bankAccountBalanceOperationFactory,
            TransactionFactory transactionFactory) {
        this.bankAccountRepository = bankAccountRepository;
        this.bankAccountBalanceOperationRepository = bankAccountBalanceOperationRepository;
        this.bankAccountBalanceOperationFactory = bankAccountBalanceOperationFactory;
        this.transactionFactory = transactionFactory;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Transaction transferBetweenBankAccounts(String fromBankAccountNumber, String toBankAccountNumber,
            Money amount) {

        List<BankAccount> bankAccounts = bankAccountRepository.findByNumberIn(
                Arrays.asList(fromBankAccountNumber, toBankAccountNumber));

        BankAccount fromBankAccount = getBankAccountByNumber(bankAccounts, fromBankAccountNumber);
        BankAccount toBankAccount = getBankAccountByNumber(bankAccounts, toBankAccountNumber);

        fromBankAccount.decreaseBalance(amount);
        fromBankAccount.checkBalanceNotNegative();
        toBankAccount.increaseBalance(amount);

        Transaction transaction = transactionFactory.createTransaction(fromBankAccount, toBankAccount, amount);

        BankAccountBalanceOperation fromBankAccountBalanceOperation =
                bankAccountBalanceOperationFactory.createDebitBankAccountBalanceOperation(transaction);
        BankAccountBalanceOperation toBankAccountBalanceOperation =
                bankAccountBalanceOperationFactory.createCreditBankAccountBalanceOperation(transaction);

        bankAccountBalanceOperationRepository.save(fromBankAccountBalanceOperation);
        bankAccountBalanceOperationRepository.save(toBankAccountBalanceOperation);

        return transaction;
    }

    private BankAccount getBankAccountByNumber(List<BankAccount> bankAccounts, String number) {
        return bankAccounts.stream()
                .filter(bankAccount -> number.equals(bankAccount.getNumber()))
                .findFirst()
                .orElseThrow(() -> new BankAccountNotFoundException("Bank account not found: " + number));
    }
}
