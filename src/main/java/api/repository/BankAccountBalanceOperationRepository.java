package api.repository;

import api.entity.BankAccountBalanceOperation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankAccountBalanceOperationRepository extends JpaRepository<BankAccountBalanceOperation, Long> {
}
