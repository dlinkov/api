package api.repository;

import api.entity.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import java.util.List;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    List<BankAccount> findByNumberIn(List<String> number);
}
