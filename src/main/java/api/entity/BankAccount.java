package api.entity;

import api.exception.InsufficientFundsException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;
import org.javamoney.moneta.Money;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "bank_account")
public class BankAccount {

    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long id;

    @Column(name = "number", length = 34, unique = true, nullable = false)
    private String number;

    private String description;

    @Columns(columns = {@Column(name = "currency"), @Column(name = "balance")})
    @Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency")
    private Money balance;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @CreationTimestamp
    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    @UpdateTimestamp
    @Column(name = "update_time", nullable = false)
    private LocalDateTime updateTime;

    public void increaseBalance(Money amount) {
        balance = balance.add(amount);
    }

    public void decreaseBalance(Money amount) {
        balance = balance.subtract(amount);
    }

    public void checkBalanceNotNegative() {
        if (balance.isNegative()) {
            throw new InsufficientFundsException("Insufficient funds in the bank account balance");
        }
    }

    @JsonIgnore
    public BigDecimal getBalanceAmount() {
        return balance.getNumberStripped();
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    @JsonProperty
    public String getNumber() {
        return number;
    }

    @JsonIgnore
    public String getDescription() {
        return description;
    }

    @JsonProperty
    public Money getBalance() {
        return balance;
    }

    @JsonIgnore
    public Customer getCustomer() {
        return customer;
    }

    @JsonIgnore
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    @JsonIgnore
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }
}
