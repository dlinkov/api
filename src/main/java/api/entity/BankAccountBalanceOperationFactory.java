package api.entity;

import org.springframework.stereotype.Component;

@Component
public class BankAccountBalanceOperationFactory {

    public BankAccountBalanceOperation createCreditBankAccountBalanceOperation(Transaction transaction) {
        return new BankAccountBalanceOperation(
                transaction,
                transaction.getToBankAccount(),
                transaction.getToBankAccount().getBalanceAmount(),
                transaction.getAmount()
        );
    }

    public BankAccountBalanceOperation createDebitBankAccountBalanceOperation(Transaction transaction) {
        return new BankAccountBalanceOperation(
                transaction,
                transaction.getFromBankAccount(),
                transaction.getFromBankAccount().getBalanceAmount(),
                transaction.getAmount().negate()
        );
    }
}
