package api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;
import org.javamoney.moneta.Money;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "from_bank_account_id", nullable = false)
    private BankAccount fromBankAccount;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "to_bank_account_id", nullable = false)
    private BankAccount toBankAccount;

    @Columns(columns = {@Column(name = "currency"), @Column(name = "amount")})
    @Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency")
    private Money amount;

    @CreationTimestamp
    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    @UpdateTimestamp
    @Column(name = "update_time", nullable = false)
    private LocalDateTime updateTime;

    public Transaction() {
    }

    Transaction(BankAccount fromBankAccount, BankAccount toBankAccount, Money amount) {
        this.fromBankAccount = fromBankAccount;
        this.toBankAccount = toBankAccount;
        this.amount = amount;
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    @JsonProperty("from_bank_account")
    public BankAccount getFromBankAccount() {
        return fromBankAccount;
    }

    @JsonProperty("to_bank_account")
    public BankAccount getToBankAccount() {
        return toBankAccount;
    }

    @JsonProperty
    public Money getAmount() {
        return amount;
    }

    @JsonProperty("create_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    @JsonProperty("update_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }
}
