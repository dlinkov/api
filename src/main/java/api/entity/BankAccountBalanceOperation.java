package api.entity;

import org.hibernate.annotations.Columns;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.javamoney.moneta.Money;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "bank_account_balance_operation")
public class BankAccountBalanceOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Columns(columns = {@Column(name = "currency"), @Column(name = "amount")})
    @Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency")
    private Money amount;

    @Column(name = "bank_account_balance", precision = 16, scale = 4, nullable = false)
    private BigDecimal bankAccountBalance;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_account_id", nullable = false)
    private BankAccount bankAccount;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "transaction_id", nullable = false)
    private Transaction transaction;

    @CreationTimestamp
    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    public BankAccountBalanceOperation() {
    }

    BankAccountBalanceOperation(Transaction transaction, BankAccount bankAccount, BigDecimal bankAccountBalance,
            Money amount) {
        this.transaction = transaction;
        this.bankAccount = bankAccount;
        this.bankAccountBalance = bankAccountBalance;
        this.amount = amount;
    }
}
