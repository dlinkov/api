package api.entity;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Component;

@Component
public class TransactionFactory {

    public Transaction createTransaction(BankAccount fromBankAccount, BankAccount toBankAccount, Money amount) {
        return new Transaction(fromBankAccount, toBankAccount, amount);
    }
}
