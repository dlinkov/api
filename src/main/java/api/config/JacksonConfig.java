package api.config;

import com.fasterxml.jackson.databind.Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.jackson.datatype.money.FieldNames;
import org.zalando.jackson.datatype.money.MoneyModule;
import org.zalando.jackson.datatype.money.QuotedDecimalNumberValueSerializer;

@Configuration
public class JacksonConfig {

    @Bean
    public Module moneyModule() {
        return new MoneyModule()
                .withFieldNames(FieldNames.valueOf("value", "currency", "formatted"))
                .withNumberValueSerializer(new QuotedDecimalNumberValueSerializer());
    }
}
