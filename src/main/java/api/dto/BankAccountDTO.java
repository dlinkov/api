package api.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BankAccountDTO {

    @NotNull
    @Size(min = 15, max = 34)
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}