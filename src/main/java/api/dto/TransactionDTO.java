package api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class TransactionDTO {

    @Valid
    @NotNull
    @JsonProperty("from_bank_account")
    private BankAccountDTO fromBankAccount;

    @Valid
    @NotNull
    @JsonProperty("to_bank_account")
    private BankAccountDTO toBankAccount;

    @Valid
    @NotNull
    private MoneyDTO amount;

    public BankAccountDTO getFromBankAccount() {
        return fromBankAccount;
    }

    public void setFromBankAccount(BankAccountDTO fromBankAccount) {
        this.fromBankAccount = fromBankAccount;
    }

    public BankAccountDTO getToBankAccount() {
        return toBankAccount;
    }

    public void setToBankAccount(BankAccountDTO toBankAccount) {
        this.toBankAccount = toBankAccount;
    }

    public MoneyDTO getAmount() {
        return amount;
    }

    public void setAmount(MoneyDTO amount) {
        this.amount = amount;
    }
}
