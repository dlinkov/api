package api.rest;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {

    public enum Code {
        INVALID_INPUT_DATA,
        INSUFFICIENT_FUNDS,
        RESOURCE_NOT_FOUND
    }

    private final Code code;
    private final String message;

    protected ErrorResponse(String message) {
        this(null, message);
    }

    protected ErrorResponse(Code code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ErrorResponse withoutCode(String message) {
        return new ErrorResponse(message);
    }

    public static ErrorResponse invalidInputData(String message) {
        return new ErrorResponse(Code.INVALID_INPUT_DATA, message);
    }

    public static ErrorResponse insufficientFunds(String message) {
        return new ErrorResponse(Code.INSUFFICIENT_FUNDS, message);
    }

    public static ErrorResponse resourceNotFound(String message) {
        return new ErrorResponse(Code.RESOURCE_NOT_FOUND, message);
    }

    public Code getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
