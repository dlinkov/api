package api.controller;

import api.dto.TransactionDTO;
import api.entity.Transaction;
import api.service.TransferService;
import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(Transaction.class)
public class TransferController {

    @Autowired
    private TransferService transferService;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/v1/transfers", method = RequestMethod.POST)
    public Resource<Transaction> transfer(@Validated @RequestBody TransactionDTO transactionDTO) {
        Transaction transaction = transferService.transferBetweenBankAccounts(
                transactionDTO.getFromBankAccount().getNumber(),
                transactionDTO.getToBankAccount().getNumber(),
                Money.of(transactionDTO.getAmount().getValue(), transactionDTO.getAmount().getCurrency())
        );

        Resource<Transaction> resource = new Resource<>(transaction);
        resource.add(
                linkTo(methodOn(getClass()).transfer(transactionDTO)).slash(transaction.getId()).withSelfRel());

        return resource;
    }
}
