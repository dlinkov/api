package api.service;

import api.entity.*;
import api.exception.BankAccountNotFoundException;
import api.repository.BankAccountBalanceOperationRepository;
import api.repository.BankAccountRepository;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.javamoney.moneta.Money;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(DataProviderRunner.class)
public class TransferServiceImplTest {

    private TransferServiceImpl transferService;

    private BankAccountRepository bankAccountRepository;
    private BankAccountBalanceOperationRepository bankAccountBalanceOperationRepository;
    private BankAccountBalanceOperationFactory bankAccountBalanceOperationFactory;
    private TransactionFactory transactionFactory;
    private Transaction transaction;
    private BankAccount fromBankAccount;
    private BankAccount toBankAccount;
    private BankAccountBalanceOperation fromBankAccountBalanceOperation;
    private BankAccountBalanceOperation toBankAccountBalanceOperation;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        bankAccountRepository = mock(BankAccountRepository.class);
        bankAccountBalanceOperationRepository = mock(BankAccountBalanceOperationRepository.class);
        bankAccountBalanceOperationFactory = mock(BankAccountBalanceOperationFactory.class);

        transactionFactory = mock(TransactionFactory.class);
        transaction = mock(Transaction.class);

        fromBankAccount = mock(BankAccount.class);
        toBankAccount = mock(BankAccount.class);
        fromBankAccountBalanceOperation = mock(BankAccountBalanceOperation.class);
        toBankAccountBalanceOperation = mock(BankAccountBalanceOperation.class);

        transferService = new TransferServiceImpl(
                bankAccountRepository,
                bankAccountBalanceOperationRepository,
                bankAccountBalanceOperationFactory,
                transactionFactory
        );
    }

    @DataProvider
    public static Object[][] transferBetweenBankAccountsData() {
        return new Object[][]{
                {
                        "A000000000000000",
                        "C000000000000000",
                        Money.of(100, "USD")
                },
                {
                        "1",
                        "2",
                        Money.of(new BigDecimal("0.99"), "RUB")
                },
        };
    }

    @UseDataProvider("transferBetweenBankAccountsData")
    @Test
    public void transferBetweenBankAccounts(
            String fromBankAccountNumber,
            String toBankAccountNumber,
            Money amount
    ) {
        // given
        given(bankAccountRepository.findByNumberIn(Arrays.asList(fromBankAccountNumber, toBankAccountNumber)))
                .willReturn(Arrays.asList(fromBankAccount, toBankAccount));

        given(fromBankAccount.getNumber())
                .willReturn(fromBankAccountNumber);

        given(toBankAccount.getNumber())
                .willReturn(toBankAccountNumber);

        given(transactionFactory.createTransaction(fromBankAccount, toBankAccount, amount))
                .willReturn(transaction);

        given(bankAccountBalanceOperationFactory.createDebitBankAccountBalanceOperation(transaction))
                .willReturn(fromBankAccountBalanceOperation);
        given(bankAccountBalanceOperationFactory.createCreditBankAccountBalanceOperation(transaction))
                .willReturn(toBankAccountBalanceOperation);

        // when
        Transaction actualTransaction = transferService.transferBetweenBankAccounts(fromBankAccountNumber,
                toBankAccountNumber, amount);

        // then
        Assert.assertEquals(transaction, actualTransaction);

        then(fromBankAccount).should().decreaseBalance(amount);
        then(fromBankAccount).should().checkBalanceNotNegative();
        then(toBankAccount).should().increaseBalance(amount);

        then(bankAccountBalanceOperationRepository).should().save(fromBankAccountBalanceOperation);
        then(bankAccountBalanceOperationRepository).should().save(toBankAccountBalanceOperation);
    }

    @DataProvider
    public static Object[][] transferBetweenBankAccountsThrowsExceptionOnBankAccountNotFoundData() {
        BankAccount fromBankAccount = mock(BankAccount.class);
        given(fromBankAccount.getNumber())
                .willReturn("A000000000000000");

        BankAccount toBankAccount = mock(BankAccount.class);
        given(toBankAccount.getNumber())
                .willReturn("C000000000000000");

        return new Object[][]{
                {
                        "A000000000000000",
                        "C000000000000000",
                        Money.of(1, "USD"),
                        Collections.emptyList()
                },
                {
                        "A000000000000000",
                        "B000000000000000",
                        Money.of(2, "USD"),
                        Collections.singletonList(fromBankAccount)
                },
                {
                        "B000000000000000",
                        "C000000000000000",
                        Money.of(3, "USD"),
                        Collections.singletonList(toBankAccount)
                },
        };
    }

    @UseDataProvider("transferBetweenBankAccountsThrowsExceptionOnBankAccountNotFoundData")
    @Test(expected = BankAccountNotFoundException.class)
    public void transferBetweenBankAccountsThrowsExceptionOnBankAccountNotFound(
            String fromBankAccountNumber,
            String toBankAccountNumber,
            Money amount,
            List<BankAccount> foundBankAccounts
    ) {
        // given
        given(bankAccountRepository.findByNumberIn(Arrays.asList(fromBankAccountNumber, toBankAccountNumber)))
                .willReturn(foundBankAccounts);

        // when
        transferService.transferBetweenBankAccounts(fromBankAccountNumber, toBankAccountNumber, amount);
    }
}
