Конвертация валют не реализована. Все счета в USD. При передаче валюты отличной от USD возникнет исключение.

Для перевода доступны счета с номерами `A000000000000000`, `B000000000000000`, `C000000000000000`. На каждом по 100USD.

## Метод для перевода денежных средств
```
POST /v1/transfers
```
##### Пример тела запроса
```
{
  "from_bank_account": {
    "number": "A000000000000000"
  },
  "to_bank_account": {
    "number": "C000000000000000"
  },
  "amount": {
    "value": 10,
    "currency": "USD"
  }
}
```
##### Пример успешного ответа
```
{

  "id": 1,
  "create_time": "2017-07-10T12:04:41.948",
  "update_time": "2017-07-10T12:04:41.948",
  "from_bank_account": {
    "number": "A000000000000000",
    "balance": {
      "value": "90",
      "currency": "USD"
    }
  },
  "to_bank_account": {
    "number": "C000000000000000",
    "balance": {
      "value": "110",
      "currency": "USD"
    }
  },
  "amount": {
    "value": "10",
    "currency": "USD"
  },
  "_links": {
    "self": {
      "href": "http://localhost:8080/v1/transfers/1"
    }
  }
}
```
##### Пример ошибки
```
{
  "code": "RESOURCE_NOT_FOUND",
  "message": "Bank account not found: Z000000000000000"
}
```